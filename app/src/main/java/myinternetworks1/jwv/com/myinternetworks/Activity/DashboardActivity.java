package myinternetworks1.jwv.com.myinternetworks.Activity;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import myinternetworks1.jwv.com.myinternetworks.Fragment_Class.HomeFragment;
import myinternetworks1.jwv.com.myinternetworks.Fragment_Class.LogRecordFragment;
import myinternetworks1.jwv.com.myinternetworks.Fragment_Class.MyBitCoinsFragment;
import myinternetworks1.jwv.com.myinternetworks.Fragment_Class.MyNetworkFragment;
import myinternetworks1.jwv.com.myinternetworks.Fragment_Class.ProfileFragment;
import myinternetworks1.jwv.com.myinternetworks.Fragment_Class.SettingsFragment;
import myinternetworks1.jwv.com.myinternetworks.R;

public class DashboardActivity extends AppCompatActivity {

    private ActionBarDrawerToggle drawerToggle;
    private String[] listTitle;
    private ListView drawerList;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

    //getting the string array of values in Strings.xml  testda
    listTitle= getResources().getStringArray(R.array.navigation_drawer_items);
    drawerList= (ListView) findViewById(R.id.list_slider);

        drawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);

    //Setting the list of arrays to the Array Adapter using simple_list_item_1 layout.
    drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, listTitle));
    drawerList.setOnItemClickListener(new DrawerItemClickListener());
    if (savedInstanceState == null) {
        selectItem(0);
    }

    //Setting the ActionbarDrawerToggle
    drawerToggle= new ActionBarDrawerToggle(this, drawerLayout,R.string.drawer_opened,
            R.string.drawer_closed) {
        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            invalidateOptionsMenu();
        }
    };

        drawerLayout.addDrawerListener(drawerToggle);

        //enabling ActionBarDrawerToggle
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }//End of OnCreate() method


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    // Sync the toggle state after onRestoreInstanceState has occurred.
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    //if the actionBarDrawerToggle is clicked, displays navigation drawer list
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)){
        }
        return true;
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);

        }
    }

    //changing the actionbar title for any items clicked in navigation drawer
    private void setActionBarTitle(int position) {
        String title;
        if (position ==0 ){
            title= getResources().getString(R.string.overview_text);
        }
        else {
            title= listTitle[position];
        }
        getSupportActionBar().setTitle(title);
    }

    //Method for switching fragment display in drawerlayout
    private void selectItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment= new HomeFragment();
                break;
            case 1:
                fragment= new MyBitCoinsFragment();
                break;
            case 2:
                fragment= new MyNetworkFragment();
                break;
            case 3:
                fragment= new ProfileFragment();
                break;
            case 4:
                fragment= new SettingsFragment();
                break;
            case 5:
                fragment= new LogRecordFragment();
                break;
            default:
                fragment=new HomeFragment();
                break;

        }


        FragmentTransaction ft= getFragmentManager().beginTransaction();
        ft.replace(R.id.frame_layout, fragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
        //set the actionbar title
        setActionBarTitle(position);
        //close drawer
        drawerLayout.closeDrawer(drawerList);
    }

}
