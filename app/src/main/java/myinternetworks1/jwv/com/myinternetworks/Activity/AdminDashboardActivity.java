package myinternetworks1.jwv.com.myinternetworks.Activity;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import myinternetworks1.jwv.com.myinternetworks.AdminDashboard_Fragments.AdminHomeFragment;
import myinternetworks1.jwv.com.myinternetworks.R;

public class AdminDashboardActivity extends AppCompatActivity {

    private ActionBarDrawerToggle adminDrawerToggle;
    private DrawerLayout adminDrawerLayout;
    private ListView adminListView;
    private String[] navTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);

        navTitles = getResources().getStringArray(R.array.admin_nav_drawer);
        adminDrawerLayout = (DrawerLayout) findViewById(R.id.admin_drawer_layout);
        adminListView = (ListView) findViewById(R.id.admin_list_slider);

        //setting the listview details to the adapter
        adminListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, navTitles));

        //setting the actionbardrawerToggle
        adminDrawerToggle = new ActionBarDrawerToggle(this, adminDrawerLayout,
                R.string.drawer_opened, R.string.drawer_closed) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        adminDrawerLayout.addDrawerListener(adminDrawerToggle);
        adminListView.setOnItemClickListener(new DrawerItemClickListener());
        if (savedInstanceState == null) {
            selectAdminItem(0);
        }
        //enabling actionbarToggle
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        adminDrawerToggle.onConfigurationChanged(newConfig);
    }
    //on Configuration Changed
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        adminDrawerToggle.syncState();
    }
    //on Options Item Selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (adminDrawerToggle.onOptionsItemSelected(item)) {
        }
        return true;
    }

    //setting the actionbar drawer toggle title name
    private void setActionBarTitle(int position) {
        String title;
        if (position == 0) {
            title= "Home";
        }
        else {
            title= navTitles[position];
        }
        getSupportActionBar().setTitle(title);
    }
    //Fragment switching
    private void selectAdminItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment= new AdminHomeFragment();
                break;
            case 1:
                break;
            default:
                break;
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
      //  ft.replace(R.id.admin_frame_layout, fragment);  //error
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();

        //set actionbar titles
        setActionBarTitle(position);
        adminDrawerLayout.closeDrawer(adminListView);

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectAdminItem(position);
        }
    }
}



