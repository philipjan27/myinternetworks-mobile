package myinternetworks1.jwv.com.myinternetworks.Activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import myinternetworks1.jwv.com.myinternetworks.Class.DBHelper;
import myinternetworks1.jwv.com.myinternetworks.R;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG= LoginActivity.class.getSimpleName();
    private Button btnLogin;
    private EditText editextUserName;
    private EditText editextPassword;
    String uName,pWord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin= (Button) findViewById(R.id.buttonLogin);
        editextUserName= (EditText) findViewById(R.id.editTextUserName);
        editextPassword= (EditText) findViewById(R.id.editTextPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if (editextUserName.getText().toString().isEmpty() || editextPassword.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please fill out needed details.",Toast.LENGTH_LONG).show();
                }else {
                    new loginUser().execute();
                }


            }
        });
    }

    /**
     * LOGIN METHOD
     */
    private class loginUser extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog;
        private boolean result;
        DBHelper db;



        @Override
        protected void onPreExecute() {

            uName= editextUserName.getText().toString().trim();
            pWord= editextPassword.getText().toString().trim();

            dialog= new ProgressDialog(LoginActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage("Logging in..");
            showDialog();
            result=false;
        }

        @Override
        protected Void doInBackground(Void... params) {

            db= new DBHelper();
            result= db.loginUser(uName,pWord);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
          /**  if(db.loginUser(uName,pWord)) {
                toastUserLogin(uName);
            }else {
                Toast.makeText(getApplicationContext(),"Login failed. Please check your credentials.",Toast.LENGTH_LONG).show();
            }*/
            hideDialog();

         if (result== true){
             toastUserLogin(uName);
         }else {
             Toast.makeText(getApplicationContext(), "Username/Password incorrect, Please double check.",Toast.LENGTH_LONG).show();
         }



        }

        private void showDialog() {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }

        private void hideDialog() {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    private void toastUserLogin(String userName) {
        Toast.makeText(LoginActivity.this,"Welcome authenticated user "+ userName+ " !",Toast.LENGTH_LONG).show();
    }


}
